# developer-cv

A `LaTeX` CV template oriented toward developers.

This is a fork of the original work by [**Jan Vorisek**](https://github.com/janvorisek/minimal-latex-cv/)
as modifed for [`LaTeX` templates](https://www.latextemplates.com/template/developer-cv).

# Preview

![preview](preview.png)

# License

This is licensed under the [MIT License](LICENSE.md).
